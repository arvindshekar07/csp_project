package vo;

public class User_VO {

	private String password;
	private String username;
	private String fname;
	private String lname;
	private String dob;
	private String address;
	private String email;
	private String phone;
	
	public User_VO(String password, String username, String fname, String lname, String dob, String address,
			String email, String phone) {
		super();
		this.password = password;
		this.username = username;
		this.fname = fname;
		this.lname = lname;
		this.dob = dob;
		this.address = address;
		this.email = email;
		this.phone = phone;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	
}
