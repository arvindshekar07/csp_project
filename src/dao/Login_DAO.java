package dao;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import vo.User_VO;

public class Login_DAO {

	DB db=null;
	
	String userName;
	String password;
	String collectionName="Login";
	DBCollection myLogin;
	 public Login_DAO(String userName, String password) {
		 this.userName =userName;
		 this.password = password;
		 db= MongoConnector.getInstance();
		 myLogin = db.getCollection(collectionName);
	 
	 }
	 
	 public boolean validate(){
		 BasicDBObject searchQuery = new BasicDBObject();
		 searchQuery.put("username", userName);
			
			//cusrsor used to find for matching input
			DBCursor cursor = myLogin.find(searchQuery);
			
			return false;
	 
	 }
	
}
